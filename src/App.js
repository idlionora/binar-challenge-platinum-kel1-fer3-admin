import { Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Dashboard from './pages/Dashboard';
import LogInPage from './pages/LogInPage';
import Cars from './pages/Cars';
import CarFormPage from './pages/CarFormPage';

function App() {
	return (
		<Routes>
			<Route path="/" element={<LogInPage />} />
			<Route path="/dashboard" element={<Dashboard />} />
			<Route path="/cars" element={<Cars />} />
			<Route path="/cars/edit/:id" element={<CarFormPage currentPage="edit" />} />
			<Route path="/cars/add-new" element={<CarFormPage currentPage="add" />} />
		</Routes>
	);
}

export default App;
