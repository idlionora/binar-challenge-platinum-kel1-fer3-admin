import React, { useState, useEffect } from "react";
import axios from "axios";
import useTable from "./useTable";
import "./TableStyle.css";
import TableFooter from "./TableFooter";

const CarsTable = ({ data, rowsPerPage }) => {
   const [carsData, setCarsData] = useState([]);
   const [page, setPage] = useState(1);
   const { slice, range } = useTable(carsData, page, rowsPerPage);

   useEffect(() => {
      axios
         .get("https://bootcamp-rent-cars.herokuapp.com/admin/v2/order", {
            headers: {
               access_token: localStorage.getItem("aDMnAcstKn"),
            },
         })
         .then((response) => {
            setCarsData(
               response.data.orders.map((order) => {
                  const modifyStartRentDate = new Intl.DateTimeFormat("id-ID", {
                     dateStyle: "full",
                     timeStyle: "long",
                     timeZone: "Asia/Jakarta",
                  }).format(new Date(order.start_rent_at));
                  const modifyFinishRentDate = new Intl.DateTimeFormat(
                     "id-ID",
                     {
                        dateStyle: "full",
                        timeStyle: "long",
                        timeZone: "Asia/Jakarta",
                     }
                  ).format(new Date(order.finish_rent_at));
                  return {
                     ...order,
                     start_rent_at: modifyStartRentDate,
                     finish_rent_at: modifyFinishRentDate,
                  };
               })
            );
         })
         .catch((error) => {
            alert(error.message);
         });
   }, []);
   return (
      <>
         <table className="table">
            <thead className="tableRowHeader">
               <tr>
                  <th className="tableHeader">No</th>
                  <th className="tableHeader">User Email</th>
                  <th className="tableHeader">Car</th>
                  <th className="tableHeader">Start Rent</th>
                  <th className="tableHeader">Finish Rent</th>
                  <th className="tableHeader">Price</th>
                  <th className="tableHeader">Category</th>
               </tr>
            </thead>
            <tbody>
               {slice.map((carData) => (
                  <tr className="tableRowItems" key={carData.id}>
                     <td className="tableCell">{carData.id}</td>
                     <td className="tableCell">{carData.User.email}</td>
                     <td className="tableCell">{carData.Car}</td>
                     <td className="tableCell">{carData.start_rent_at}</td>
                     <td className="tableCell">{carData.finish_rent_at}</td>
                     <td className="tableCell">{carData.total_price}</td>
                     <td className="tableCell">{carData.language}</td>
                  </tr>
               ))}
            </tbody>
         </table>
         <TableFooter
            range={range}
            slice={slice}
            setPage={setPage}
            page={page}
         />
      </>
   );
};

export default CarsTable;
