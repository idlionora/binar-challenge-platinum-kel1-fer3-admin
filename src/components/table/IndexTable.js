import React from "react";
import "./TableStyle.css";
import Table from "./CarsTable";

const IndexTable = () => {
   return (
      <main className="container">
         <div className="wrapper">
            <Table rowsPerPage={5} />
         </div>
      </main>
   );
};

export default IndexTable;
