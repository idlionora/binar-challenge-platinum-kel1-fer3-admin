import { Col, Container, Row } from "react-bootstrap";
import GrapichCar from "../components/GrapichCar";
import NavbarComp from "../components/Navbar";
import IndexTable from "../components/table/IndexTable";

const Dashboard = () => {
   return (
      <Container
         fluid
         className="p-0 m-0"
         style={{ minHeight: "100vh", background: "#F4F5F7" }}
      >
         <NavbarComp currentPage="dashboard" />
         <Row className="m-0">
            <Col
               xs="auto"
               className="d-none d-md-block h-100"
               style={{ width: "20.14%", minWidth: "218px", maxWidth: "290px" }}
            ></Col>
         </Row>
         <div
            className="dataGraphTable"
            style={{ marginLeft: "320px", marginTop: "30px" }}
         >
            <GrapichCar />
            <br />
            <IndexTable />
         </div>
      </Container>
   );
};

export default Dashboard;
