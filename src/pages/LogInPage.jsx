import { Button, Container, Form } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import carBackground from '../assets/car-background.png';
import axios from 'axios';


const LogInPage = () => {
	const [validated, setValidated] = useState(false);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [submittedForm, setSubmittedForm] = useState({});
	const [hasNotRegistered, setHasNotRegistered] = useState(false);
	const [wrongPass, setWrongPass] = useState(false);

	const navigate = useNavigate();
	useEffect(() => {
		if (localStorage.getItem('aDMnAcstKn') !== null) navigate('/dashboard');
	}, []);

	async function signIn() {
		try {
			const payload = { email, password };
			const response = await axios.post(
				'https://bootcamp-rent-cars.herokuapp.com/admin/auth/login',
				payload
			);
			const adminLog = response.data;
			if (response.status === 201) {
				localStorage.setItem('aDMnAcstKn', adminLog.access_token);
				navigate('/dashboard');
			}
		} catch (err) {
			console.error(err);
			const errorStatus = err.response.status;
			if (errorStatus === 404) setHasNotRegistered(true);
			if (errorStatus === 400) setWrongPass(true);
		}
	}

	const submitSignIn = (event) => {
		event.preventDefault();
		const form = event.currentTarget;
		!!form.checkValidity() && signIn();

		setSubmittedForm({
			email: email,
			password: password,
		});
		setValidated(true);
	};

	if (!!hasNotRegistered && email !== submittedForm.email) setHasNotRegistered(false);
	if (!!wrongPass && password !== submittedForm.password) setWrongPass(false);

	return (
		<Container
			fluid
			className="w-100 p-0 overflow-hidden position-relative"
			style={{ background: 'skyblue', height: '100vh' }}
		>
			<div
				className="h-100 position-absolute"
				style={{
					backgroundImage: `url(${carBackground})`,
					backgroundPosition: 'right',
					backgroundRepeat: 'no-repeat',
					backgroundSize: 'cover',
					right: '17.85%',
					width: '82.15%',
				}}
			/>
			<div
				className="h-100 position-absolute login-panel d-flex justify-content-center align-items-center"
				style={{ background: '#fff', right: 0 }}
			>
				<div className="mx-3 w-100" style={{ maxWidth: '23.125rem' }}>
					<div className="binar-logo" />
					<h1
						style={{
							fontFamily: 'Arial',
							fontWeight: 700,
							fontSize: '1.5rem',
							lineHeight: 1.5,
							marginBottom: '2rem',
						}}
					>
						Welcome, Admin BCR
					</h1>
					{(!!hasNotRegistered || !!wrongPass) && (
						<div
							style={{
								padding: '12px 25px',
								background: 'rgba(208, 12, 26, 0.1)',
								borderRadius: '5px',
								fontFamily: 'Arial, sans-serif',
								fontSize: '0.75rem',
								lineHeight: 1.5,
								marginBottom: '2rem',
								color: '#D00C1A',
							}}
						>
							Masukkan username dan password yang benar. Perhatikan penggunaan huruf
							kapital.
						</div>
					)}
					<Form noValidate validated={validated} onSubmit={submitSignIn}>
						<fieldset>
							<Form.Group controlId="email">
								<Form.Label
									style={{
										fontFamily: 'Arial, sans-serif',
										fontSize: '0.875rem',
										lineHeight: '1.4286',
									}}
								>
									Email
								</Form.Label>
								<Form.Control
									type="email"
									placeholder="Contoh: johndee@gmail.com"
									className={hasNotRegistered && 'tab--invalid'}
									style={{
										fontFamily: 'Arial, sans-serif',
										fontSize: '0.75rem',
										lineHeight: 1.5,
										padding: '10px 15px',
									}}
									required
									onChange={(e) => setEmail(e.target.value)}
								></Form.Control>
								<Form.Control.Feedback type="invalid">
									Masukkan email yang benar
								</Form.Control.Feedback>
							</Form.Group>
							<Form.Group controlId="password" className="mt-3">
								<Form.Label
									style={{
										fontFamily: 'Arial, sans-serif',
										fontSize: '0.875rem',
										lineHeight: '1.4286',
									}}
								>
									Password
								</Form.Label>
								<Form.Control
									type="password"
									minLength="6"
									placeholder="6+ karakter"
									className={(hasNotRegistered || wrongPass) && 'tab--invalid'}
									style={{
										fontFamily: 'Arial, sans-serif',
										fontSize: '0.75rem',
										lineHeight: 1.5,
										padding: '10px 15px',
									}}
									required
									onChange={(e) => setPassword(e.target.value)}
								></Form.Control>
								<Form.Control.Feedback type="invalid">
									Masukan password minimal 6 karakter
								</Form.Control.Feedback>
							</Form.Group>
							<Button
								type="submit"
								className="w-100"
								style={{
									borderRadius: '2px',
									background: '#0D28A6',
									marginTop: '2rem',
								}}
							>
								Sign In
							</Button>
						</fieldset>
					</Form>
				</div>
			</div>
		</Container>
	);
};

export default LogInPage;
